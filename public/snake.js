var tmr=false,					// Timer
	canvas,						// Canvas
	ctx,						// Canvas 2d context
	startStop,					// Game start/stop button
	score,						// Score element
	snake,						// Object for snake data
	diamond,					// Object for diamond data
	YB=30,						// 
	XB=40,						// 
	ph,							// 
	pw,							// 
	highScore=0,				// Variable for keeping highscore
	scoreV=0,					// 
	speedSlowRadio,				// Radio button for selecting slow speed
	speedMediumRadio,			// Radio button for selecting medium speed
	speedFastRadio,				// Radio button for selecting fast speed
	gamespeed=300,				// Miliseconds delay
	incrSpeed					// Checkbox for increasing speed on every eat
;

/**
 * Function for doing what's needed on game over.
 */
function gameOver() {
	clearTimeout(tmr);
	tmr=false;
	if(highScore<scoreV){
		highScore = scoreV;
		document.getElementById('highscore').innerText = highScore;
	}
	scoreV=0;
	document.getElementById('game-messages').innerText = 'gameOver';
	startStop.innerText='Start';
	resetGame ();
}

/**
 * Function for placing diamond
 */
function placeDiamond (){
	diamond={x:0,y:0};
	var nf=true;
	while (nf){
		nf=false;
		diamond.x=Math.floor(Math.random()*XB);
		diamond.y=Math.floor(Math.random()*YB);
		for(var i=0; i<snake.parts.length; i++) if(diamond.x == snake.parts[i].x && diamond.y == snake.parts[i].y){
			nf=true;
			break;
		}
	}
	ctx.fillStyle="#ff0000";
	ctx.fillRect(diamond.x*pw, diamond.y*ph,pw,ph);
	ctx.fillStyle="#000000";
}

/**
 * Function for performing actions for each snake step.
 */
function play() {
	clearTimeout(tmr);

	snake.dir.nx = snake.dir.x;
	snake.dir.ny = snake.dir.y;
	var nfx=snake.parts[0].x+snake.dir.x, nfy=snake.parts[0].y+snake.dir.y;
	if(
		nfx == diamond.x && nfy == diamond.y
	) {
		scoreV++;
		score.innerText = scoreV;
		var lprt = snake.parts[snake.parts.length-1];
		snake.parts.push({x:lprt.x,y:lprt.y});
		if(incrSpeed.checked)gamespeed-=gamespeed/50;
		placeDiamond();
	} else ctx.clearRect(
		snake.parts[snake.parts.length-1].x*pw,
		snake.parts[snake.parts.length-1].y*ph,
		pw,ph
	);

	for(var i=snake.parts.length-1; i>0; i--) {
		snake.parts[i].x = snake.parts[i-1].x;
		snake.parts[i].y = snake.parts[i-1].y;
		if(
			snake.parts[i].x == nfx &&
				snake.parts[i].y == nfy
		) { gameOver(); return; }
	}
	snake.parts[0].x+=snake.dir.x;
	snake.parts[0].y+=snake.dir.y;

	if (document.getElementById('loopBorders').checked === false){
		if(
			snake.parts[0].x<0 ||
				snake.parts[0].y<0 ||
				snake.parts[0].x > XB-1 ||
				snake.parts[0].y > YB-1
		) {gameOver(); return;}
	} else {
		if(snake.parts[0].x<0)
			snake.parts[0].x = XB-1;
		else if (snake.parts[0].x > XB-1)
			snake.parts[0].x = 0;
		else if (snake.parts[0].y<0)
			snake.parts[0].y=YB;
		else if (snake.parts[0].y > YB-1)
			snake.parts[0].y=0;
	}
	ctx.fillRect(snake.parts[0].x*pw,snake.parts[0].y*ph,pw,ph);
	if(snake.start !== false)tmr=setTimeout("play();",gamespeed);
}

/**
 * Function for reseting game.
 */
function resetGame () {
	score.innerText = 0;
	snake={
		dir:{x:1,y:0,nx:1,ny:0},
		start:false,
		parts:[]
	};
	snake.parts.push({x:0,y:0});
	ctx.clearRect(0,0,canvas.width,canvas.height);
	ctx.fillRect(0, 0,pw,ph);
	placeDiamond();
}

/**
 * Function for showing/hidding options screen.
 */
function showHideOptions() {
	snake.start=false;
	document.getElementById('startstopbtn').innerText='Continue';
	document.getElementById('game-messages').innerText='Paused';
	clearTimeout(tmr);
	tmr = false;
	if (document.getElementById('options').style.display == 'none') {
		document.getElementById('options').style.display =  'block';
		document.getElementById('game-container').style.display = 'none';
		document.getElementById('startstopbtn').style.display = 'none';
		document.getElementById('optionsbtn').innerText = 'Go Back';
	} else {
		document.getElementById('options').style.display =  'none';
		document.getElementById('game-container').style.display =  'block';
		document.getElementById('startstopbtn').style.display = '';
		document.getElementById('optionsbtn').innerText = 'Options';
		document.getElementById('mycanvas').focus();
	}
}

function loadgame () {
	canvas=document.getElementById('mycanvas');
	canvas.height=300;
	canvas.width=400;
	canvas.tabIndex = 1;
	ph = canvas.height / YB;
	pw = canvas.width / XB;
	ctx=canvas.getContext('2d');
	startStop=document.getElementById('startstopbtn');
	score=document.getElementById('score');

	incrSpeed = document.getElementById('incSpeedOnEat');
	speedSlowRadio=document.getElementById('speedSlow');
	speedMediumRadio=document.getElementById('speedMedium');
	speedFastRadio=document.getElementById('speedFast');
	document.getElementById('game-messages').innerText='Paused';


	switch ( true ) {
	case speedSlowRadio.checked:gamespeed=300;break;
	case speedMediumRadio.checked:gamespeed=200;break;
	case speedFastRadio.checked:gamespeed=100;break;
	}

	speedSlowRadio.addEventListener('click',function(e){ gamespeed=300; });
	speedMediumRadio.addEventListener('click',function(e){ gamespeed=200; });
	speedFastRadio.addEventListener('click',function(e){ gamespeed=100; });

	document.getElementById('optionsbtn').addEventListener('click', showHideOptions);

	startStop.addEventListener('click',function(e){
		switch(e.target.innerText){
		case"Start":
			clearTimeout(tmr);
			resetGame();
			snake.start=true;
			e.target.innerText='Pause';
			document.getElementById('game-messages').innerText='Playing';
			tmr=setTimeout("play();",gamespeed);
			document.getElementById('mycanvas').focus();
			break;

		case"Pause":
			snake.start=false;
			document.getElementById('game-messages').innerText='Paused';
			e.target.innerText='Continue';
			clearTimeout(tmr);
			tmr = false;
			break;

		case"Continue":
			snake.start=true;
			document.getElementById('game-messages').innerText='Playing';
			tmr=setTimeout("play();",gamespeed);
			e.target.innerText='Pause';
			document.getElementById('mycanvas').focus();
			break;
		}
	});

	document.getElementById('goLeft').addEventListener('click', function () {
		if (snake.dir.nx !== 1){
			snake.dir.x = -1;
			snake.dir.y = 0;
		}
	});

	document.getElementById('goRight').addEventListener('click', function () {
		if (snake.dir.nx !== -1){
			snake.dir.x = 1;
			snake.dir.y = 0;
		}
	});

	document.getElementById('goUp').addEventListener('click', function () {
		if (snake.dir.ny !== 1){
			snake.dir.x = 0;
			snake.dir.y = -1;
		}
	});

	document.getElementById('goDown').addEventListener('click', function () {
		if (snake.dir.ny !== -1){
			snake.dir.x = 0;
			snake.dir.y = 1;
		}
	});

	document.addEventListener('keydown',function(e){
		//document.getElementById('score').innerText = e.keyCode;
		switch(e.keyCode){
		case 37:
			if (snake.dir.nx !== 1){
				snake.dir.x = -1;
				snake.dir.y = 0;
			}
			break;

		case 38:
			if (snake.dir.ny !== 1){
				snake.dir.x = 0;
				snake.dir.y = -1;
			}
			break;

		case 39:
			if (snake.dir.nx !== -1){
				snake.dir.x = 1;
				snake.dir.y = 0;
			}
			break;

		case 40:
			if (snake.dir.ny !== -1){
				snake.dir.x = 0;
				snake.dir.y = 1;
			}
			break;

		case 79:
			showHideOptions();
			break;

		case 80:
			if(tmr!==false){
				snake.start=false;
				document.getElementById('game-messages').innerText='Paused';
				startStop.innerText='Continue';
				clearTimeout(tmr); tmr = false;
			}else{
				snake.start=true;
				document.getElementById('game-messages').innerText='Playing';
				startStop.innerText='Pause';
				document.getElementById('mycanvas').focus();
				tmr=setTimeout("play();",gamespeed);
			}
			break;

		default:
			console.log(e.keyCode);
			break;
			
		}
	});

	resetGame ();
	//play();
}

window.addEventListener('load', function() {
	loadgame();
});
