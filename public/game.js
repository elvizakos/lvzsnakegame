var url = new URL(document.currentScript.src);
var c = url.searchParams.get("$");
if ( c !== null ) window[c].lvzsnakegame = {
	loadApp : function ( app ) {

		var game = new (function() {
			this.elements={};
			this.elements.container = document.createElement('div');
			this.elements.container.style.overflow = 'hidden';

			this.elements.gamecontainer = document.createElement('div');
			this.elements.leftcontainer = document.createElement('div');
			this.elements.rightcontainer = document.createElement('div');
			this.elements.centercontainer = document.createElement('div');
			this.elements.goleft = document.createElement('div');
			this.elements.goup = document.createElement('div');
			this.elements.goright = document.createElement('div');
			this.elements.godown = document.createElement('div');
			this.elements.canvas = document.createElement('canvas');

			// this.elements.startstopbtn = document.createElement('button');
			// this.elements.optionsbtn = document.createElement('button');
			// this.elements.startstopbtn.id = 'startstopbtn';
			// this.elements.optionsbtn.id = 'optionsbtn';

			this.elements.score = document.createElement('span');
			this.elements.score.id = 'score';
			this.elements.scorelbl = document.createElement('div');
			this.elements.scorelbl.appendChild(document.createTextNode('Score: '));
			this.elements.scorelbl.appendChild(this.elements.score);

			this.elements.highscore = document.createElement('span');
			this.elements.highscorelbl = document.createElement('div');
			this.elements.highscore.id = 'highscore';
			this.elements.highscorelbl.appendChild(document.createTextNode('Highscore: '));
			this.elements.highscorelbl.appendChild(this.elements.highscore);

			this.elements.gamemessages = document.createElement('div');
			this.elements.gamemessages.id = 'game-messages';

			this.elements.container.appendChild(this.elements.gamecontainer);

			this.elements.gamecontainer.appendChild(this.elements.leftcontainer);
			this.elements.gamecontainer.appendChild(this.elements.centercontainer);
			this.elements.gamecontainer.appendChild(this.elements.rightcontainer);

			this.elements.leftcontainer.appendChild(this.elements.goleft);
			this.elements.rightcontainer.appendChild(this.elements.goright);

			this.elements.centercontainer.appendChild(this.elements.goup);
			this.elements.centercontainer.appendChild(this.elements.canvas);
			this.elements.centercontainer.appendChild(this.elements.godown);

			this.elements.gamecontainer.id = 'game-container';
			this.elements.leftcontainer.id = 'left-container';
			this.elements.rightcontainer.id = 'right-container';
			this.elements.centercontainer.id = 'center-container';
			this.elements.goleft.id = 'goLeft';
			this.elements.goup.id = 'goUp';
			this.elements.godown.id = 'goDown';
			this.elements.goright.id = 'goRight';
			this.elements.canvas.id = 'mycanvas';
			this.elements.canvas.style.display = 'block';
			this.elements.canvas.style.width = '400px';
			this.elements.canvas.style.height = '300px';
			this.elements.canvas.setAttribute('width','400');
			this.elements.canvas.setAttribute('height','300');

			this.elements.optionscontainer = document.createElement('div');
			this.elements.optionscontainer.id = 'options';
			this.elements.optionscontainer.style.display = 'none';
			this.elements.optionsframe1 = document.createElement('div');
			this.elements.optionsframe1.classList.add('options');
			this.elements.optionsframe1.style.cssFloat = 'left';
			this.elements.optionsframe1.style.margin = '0 20px 0 0';
			this.elements.loopborderslbl = document.createElement('label');
			this.elements.loopborderschk = document.createElement('input');
			this.elements.incspeedlbl = document.createElement('label');
			this.elements.incspeedchk = document.createElement('input');

			this.elements.fieldset1 = document.createElement('fieldset');
			this.elements.fieldset1legend = document.createElement('legend');
			this.elements.setspeedslowlbl = document.createElement('label');
			this.elements.setspeedslowopt = document.createElement('input');
			this.elements.setspeedmediumlbl = document.createElement('label');
			this.elements.setspeedmediumopt = document.createElement('input');
			this.elements.setspeedfastlbl = document.createElement('label');
			this.elements.setspeedfastopt = document.createElement('input');

			this.elements.container.appendChild(this.elements.optionscontainer);

			this.elements.optionscontainer.appendChild(this.elements.optionsframe1);
			this.elements.optionsframe1.appendChild(this.elements.loopborderslbl);

			this.elements.loopborderslbl.appendChild(this.elements.loopborderschk);
			this.elements.loopborderschk.id = 'loopBorders';
			this.elements.loopborderschk.type = 'checkbox';
			this.elements.loopborderslbl.appendChild(document.createTextNode(' On border touch, transport to the opposite side'));

			this.elements.optionsframe1.appendChild(this.elements.incspeedlbl);
			this.elements.incspeedlbl.appendChild(this.elements.incspeedchk);
			this.elements.incspeedchk.id = 'incSpeedOnEat';
			this.elements.incspeedchk.type = 'checkbox';
			this.elements.incspeedlbl.appendChild(document.createTextNode(' Increase speed on every diamond eat.'));

			this.elements.optionsframe1.appendChild(this.elements.fieldset1);
			this.elements.fieldset1.appendChild(this.elements.fieldset1legend);
			this.elements.fieldset1legend.appendChild(document.createTextNode('Snake speed'));

			this.elements.fieldset1.appendChild(this.elements.setspeedslowlbl);
			this.elements.setspeedslowlbl.appendChild(this.elements.setspeedslowopt);
			this.elements.setspeedslowopt.id = 'speedSlow';
			this.elements.setspeedslowopt.name = 'speed';
			this.elements.setspeedslowopt.type = 'radio';
			this.elements.setspeedslowopt.checked = true;
			this.elements.setspeedslowlbl.appendChild(document.createTextNode('Slow'));

			this.elements.fieldset1.appendChild(this.elements.setspeedmediumlbl);
			this.elements.setspeedmediumlbl.appendChild(this.elements.setspeedmediumopt);
			this.elements.setspeedmediumopt.id = 'speedMedium';
			this.elements.setspeedmediumopt.name = 'speed';
			this.elements.setspeedmediumopt.type = 'radio';
			this.elements.setspeedmediumlbl.appendChild(document.createTextNode('Medium'));

			this.elements.fieldset1.appendChild(this.elements.setspeedfastlbl);
			this.elements.setspeedfastlbl.appendChild(this.elements.setspeedfastopt);
			this.elements.setspeedfastopt.id = 'speedFast';
			this.elements.setspeedfastopt.name = 'speed';
			this.elements.setspeedfastopt.type = 'radio';
			this.elements.setspeedfastlbl.appendChild(document.createTextNode('Fast'));

		})();

		var self = lvzwebdesktop(app.container);

		if ( self === false ) {
			app.container.appendChild(game.elements.container);
		}
		else {

			if ( self.type == 'window' ) {
				self.attachItem(game.elements.container)
					.setTitle('LVzSnakeGame')
					.setLayout('menu:minimize,close')
					.disableResize()
					.resizeOnContent(false)
					.setWidth(502)
					.setHeight(402)
				;
				self.props.maximizable = false;
				self.props.resizable = false;
				self.props.disableBorder = true;

				// Menubar
				var mb;
				if ( typeof self.menubar == 'object' && self.menubar !== null )
					mb = self.menubar;
				else {
					mb = new lvzwebdesktop.MenubarObject();
				}
				mb.attachTo(self,'top');

				var gmenu = mb.createMenuItem('Start');
				var omenu = mb.createMenuItem('Options');
				var hmenu = mb.createMenuItem('Help');

				gmenu.id = 'startstopbtn';
				omenu.id = 'optionsbtn';

				self.game = game;

				mb.addMenuItem(gmenu)
					.addMenuItem(omenu)
					.addMenuItem(hmenu)
				;

				// Build window's statusbar
				var s;
				if ( typeof self.statusbar == 'object' && self.statusbar !== null )
					s = self.statusbar;
				else {
					s = new lvzwebdesktop.StatusbarObject();
					s.attachToWindow(self);
				}

				s.addItem(game.elements.gamemessages);
				s.addItem(game.elements.scorelbl);
				s.addItem(game.elements.highscorelbl);

				window.loadgame();

				window.showHideOptions = function () {
					var self = showHideOptions.self;
					if ( self === null || self === false ) return;
					self = self.getParentOfType('WindowObject');
					if ( self === false ) return;
					self.game.elements.gamemessages.innerText='Paused';
					showHideOptions.gmenu.firstElementChild.innerText = 'Continue';
					if ( self.game.elements.gamecontainer.style.display != 'none' ) {
						self.game.elements.gamecontainer.style.display = 'none';
						self.game.elements.optionscontainer.style.display = '';
						showHideOptions.omenu.firstElementChild.innerText = 'Go back';
						showHideOptions.gmenu.style.display = 'none';
						clearTimeout(tmr);
						tmr = false;
					}
					else {
						self.game.elements.gamecontainer.style.display = '';
						self.game.elements.optionscontainer.style.display = 'none';
						showHideOptions.omenu.firstElementChild.innerText = 'Options';
						showHideOptions.gmenu.style.display = '';
						self.game.elements.canvas.focus();
					}
				};

				window.showHideOptions.self = self;
				window.showHideOptions.omenu = omenu;
				window.showHideOptions.gmenu = gmenu;

				// omenu.removeEventListener('click',showHideOptions);
				// omenu.addEventListener('click', );

			}
		}
	}
};
else window.addEventListener('load', loadgame);
